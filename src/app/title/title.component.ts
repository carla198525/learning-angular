import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'my-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  public title: string;
  public comment: string;
  public year: number;

  constructor() {
    this.title = 'Hola soy mi componente NUEVO';
    this.comment = 'Este es mi primer componente';
    this.year = 2019;
  }

  ngOnInit() {
    console.log("Cargando componente nuevo!!");
  }

}
